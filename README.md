# Gitlab Runner

How to install gitlab runner on a virtual machine.

## Prerequisites

If you want to use the Docker executor, make sure to install Docker before using GitLab Runner.

## Installation on linux

Follow the instructions on the [official documentation](https://docs.gitlab.com/runner/install/linux-repository.html).

![Installation](images/runner_installation.png)

## Registration with docker executor

![docker executor](images/docker_executor.png)

## Resgistration with shell executor

![shell executor](images/shell_executor.png)

## Verification on Gitlab

![runners](images/proxmox_runners.png)